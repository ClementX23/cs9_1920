#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Turtles, fractals, and recursion
"""
import turtle
import random 


def polygon(sides, side_length):
    """
    Draw a polygon.
    Args:
        sides: The total number of sides of the polygon
        side_length: The side-length of the polygon
    """
    polygon_helper(sides,side_length,0)
    
def polygon_helper(sides, side_length, sides_yet):
    """
    Draw a polygon.
    
    We can draw a polygon recursively by doing the following until
    sides_yet reaches sides:
        0. Draw a segment of the specified side_length
        1. Turn left (360/sides) degrees.
        2. Use polygon_helper with sides_yet increased by 1.
        
    Args:
        sides: The total number of sides of the polygon
        side_length: The side-length of the polygon
    """
    # BEGIN SOLUTION 
    turtle.pendown()
    turtle.begin_fill()
    turtle.fd(side_length)
    turtle.left(360/sides)
    if sides_yet <= sides:
        polygon_helper(sides,side_length,sides_yet+1)
    else:
        turtle.end_fill()
        turtle.penup()
    # END SOLUTION
    
def spiral(initial_length, angle, multiplier):
    """
    Draw a spiral.
    
    Drawing a spiral can be done by recursively doing the following until
    side length is too small (less than 1) or too big (greater than 200):
        0. Draw a segment of given length
        1. Rotate left by angle.
        2. Draw a spiral with (initial_length * multiplier) as the length
            of the "initial" segment, angle as the angle, and multiplier
            as the multiplier.
    
    Args:
        initial_length: The length of the first segment of the spiral
        angle: The angle to turn before drawing the next segment
        multiplier: The amount to multiply the current length by
            to obtain the length of the next segment
    """
    turtle.pd()
    
    turtle.fd(initial_length)
    turtle.lt(angle)
    if 1 < initial_length < 100:
        spiral(initial_length * multiplier,angle,multiplier)
    else:
        turtle.pu()
    # END SOLUTION
    
def tree(trunk_length, levels):
    """
    Draw a recursive tree with a branching structure.
    
    Args:
        trunk_length: The length of the trunk of the tree
        levels: The total number of recursive levels to be used when drawing
    """
    # BEGIN SOLUTION  
    turtle.lt(90)
    tree_helper(trunk_length, levels, 0,random.randint(10,15))
    # END SOLUTION
    
def tree_helper(trunk_length, levels, levels_now, angle):
    turtle.pd()
    turtle.fd(trunk_length)
    if levels_now <= levels:
        turtle.lt(angle)
        tree_helper(trunk_length-random.randint(1,5), levels, levels_now + random.randint(1,2),random.randint(10,15))
        turtle.rt(angle*2)
        tree_helper(trunk_length-random.randint(1,5), levels, levels_now + random.randint(1,2),random.randint(10,15))
        turtle.lt(angle)
    else:
        turtle.pd()
    turtle.bk(trunk_length)
    
    
def minkowski(side_length, levels):
    """
    Draw a Minkowski curve.
    
    A "true" (infinitely recursive) Minkowski curve is constructed as follows:
        0. Draw a Minkowski curve with 1/4 the side length
        1. Make a 90 degree left turn.
        2. Draw a Minkowski curve with 1/4 the side length
        3. Make a 90 degree right turn
        4. Draw a Minkowski curve with 1/4 the side length
        5. Make a 90 degree right turn
        6. Draw a Minkowski curve with 1/4 the side length
        7. Draw a Minkowski curve with 1/4 the side length
        8. Make a 90 degree left turn.
        9. Draw a Minkowski curve with 1/4 the side length
        10. Make a 90 degree left turn.
        11. Draw a Minkowski curve with 1/4 the side length
        12. Make a 90 degree right turn.
        13. Draw a Minkowski curve with 1/4 the side length
    
    Args:
        side_length: The side-length of the curve
        levels: The total number of recursive levels to be used when drawing
    """
    # BEGIN SOLUTION
    if levels != 0:
        minkowski(side_length/4, levels-1)
        turtle.lt(90)
        minkowski(side_length/4, levels-1)
        turtle.rt(90)
        minkowski(side_length/4, levels-1)
        turtle.rt(90)
        minkowski(side_length/4, levels-1)
        minkowski(side_length/4, levels-1)
        turtle.lt(90)
        minkowski(side_length/4, levels-1)
        turtle.lt(90)
        minkowski(side_length/4, levels-1)
        turtle.rt(90)
        minkowski(side_length/4, levels-1)
    else:
        turtle.fd(side_length)
    pass
    # END SOLUTION
    
def snowflake(side_length, levels):
    """
    Draw a Koch snowflake composed of three Koch curves arranged as if they
    were sides of an equilateral triangle.
    
    Args:
        side_length: The side-length of the curve
        levels: The total number of recursive levels to be used when drawing
    """
    # BEGIN SOLUTION
    if levels != 0:
        snowflake_side(side_length, levels - 1)
        turtle.rt(120)
        snowflake_side(side_length, levels - 1)
        turtle.rt(120)
        snowflake_side(side_length, levels - 1)
        turtle.rt(120)
    else:
        turtle.fd(side_length)
    # END SOLUTION


def snowflake_side(side_length, levels):
    """
    Draw a finite Koch curve which might be used to form one side of a snowflake.
    
    A "true" (infinitely recursive) Koch curve is constructed as follows:
        0. Draw a Koch curve with 1/3 the side length
        1. Turn left 60 degrees.
        2. Draw a Koch curve with 1/3 the side length
        3. Turn right 120 degrees.
        4. Draw a Koch curve with 1/3 the side length
        5. Turn left 60 degrees.
        6. Draw a Koch curve with 1/3 the side length
    
    
    A Koch curve 
    Args:
        side_length: The side-length of the curve
        levels: The total number of recursive levels to be used when drawing
    """
    # BEGIN SOLUTION
    if levels != 0:
        snowflake_side(side_length/3,levels - 1)
        turtle.lt(60)
        snowflake_side(side_length/3,levels - 1)
        turtle.rt(120)
        snowflake_side(side_length/3,levels - 1)
        turtle.lt(60)
        snowflake_side(side_length/3,levels - 1)
    else:
        turtle.fd(side_length)
    # END SOLUTION

def main():
    # BEGIN POLYGON TESTING
    print("Polygon testing:")
    while True:
        answer = input("Number of sides? ")
        if not answer.isdigit():
            break
        sides = int(answer)
        answer = input("Side length? ")
        if not answer.isdigit():
            break
        length = int(answer)
        turtle.reset()
        polygon(sides, length)
    # END POLYGON TESTING
    # BEGIN SPIRAL TESTING
    print("Spiral testing:")
    while True:
        answer = input("Side length? ")
        if not answer.isdigit():
            break
        length = int(answer)
        answer = input("Angle? ")
        if not answer.isdigit():
            break
        angle = int(answer)
        multiplier = float(input("Multiplier? "))
        turtle.reset()
        spiral(length, angle, multiplier)
    # END SPIRAL TESTING
    # BEGIN TREE TESTING
    print("Tree testing:")
    while True:
        answer = input("Trunk length?")
        if not answer.isdigit():
            break
        length = int(answer)
        answer = input("Levels? ")
        if not answer.isdigit():
            break
        levels = int(answer)
        turtle.reset()
        tree(length, levels)
    # END TREE TESTING
    # BEGIN MINKOWSKI TESTING
    print("MINKOWSKI testing:")
    while True:
        answer = input("Length? ")
        if not answer.isdigit():
            break
        length = int(answer)
        answer = input("Levels? ")
        if not answer.isdigit():
            break
        levels = int(answer)
        turtle.reset()
        minkowski(length, levels)
    # END MINKOWSKI TESTING
    # BEGIN SNOWFLAKE TESTING
    print("Snowflake testing:")
    while True:
        answer = input("Length? ")
        if not answer.isdigit():
            break
        length = int(answer)
        answer = input("Levels? ")
        if not answer.isdigit():
            break
        levels = int(answer)
        turtle.reset()
        snowflake(length, levels)
    # END SNOWFLAKE TESTING
    




# Don't think too hard about what these lines do.
# Just make sure not to edit them.
if __name__ == '__main__': 
    main()