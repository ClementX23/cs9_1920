git status : check on the status of the repo including the changed files, the files in the staging area to be committed, the number of commits since push, etc.

git add foo: adds the file foo (that's an example file) to the staging area so that it can be committed

git commit: makes a commit out of the files in the staging area. This will open up a text editor, vim see help on vim below

git push: puts your local commits to a remote copy of the repo

git pull: gets remote commits and puts them on your local copy of the repo.

origin: the copy of the repo on Bitbucket that is associated with your account

upstream: my copy of the repo on Bitbucket

master: the default "branch" in git. 

-----------
Vim help:
Vim is a weird text editor. Since it's made for  command line interface, if we want to be able to do nice, fancy standard text editor things we need to be able to type commands to do that. That's inconvenient because we also need to be able type actual text. The way this is handled is that there are two modes: command mode and INSERT MODE. 

INSERT MODE: You can access INSERT MODE by hitting the i key. When you do this, --INSERT MODE-- should show up in the bottom left corner of the window. INSERT MODE is where you type normal stuff in the document. You an leave INSERT MODE by hitting the ESC key.

Command mode: This is the default mode. You can access command mode by hitting the ESC key. Command mode is where you communicate with your text editor by typing things.

You can save and exit a file you're editing in Vim by making sure you're in command mode and holding the Shift key while you press the z key twice.
